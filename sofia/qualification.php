<?php

require_once('../../config.php');
require_once('lib.php');


$PAGE->set_url('/report/sofia/qualification.php');

    
//if not id
require_login();
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_title(get_string('course_qualification', 'report_sofia')); 
$PAGE->set_heading(get_string('report')); // set heading

echo $OUTPUT->header();

$menu = 'qualification';
?>

<?php  require_once(dirname(__FILE__) . '/includes/header.php');  ?>



<div id="table_div" style="margin-top: 30px;"></div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {packages:['table']});
	google.charts.setOnLoadCallback(drawChart);
	function drawChart() 
	{
		var data = new google.visualization.DataTable();
		data.addColumn('number', 'Course Id');
		data.addColumn('string', 'Course Name');
		data.addColumn('number', 'Previous Course Id');
		data.addColumn('string', 'Previous Course');
		data.addRows(<?php echo sofia_report_get_enrol_qualification(); ?>);
		var table = new google.visualization.Table(document.getElementById('table_div'));
		table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
	}
    </script>


<?php
echo $OUTPUT->footer();
