<?php


defined('MOODLE_INTERNAL') || die;


function sofia_report_extend_navigation($navigation, $course, $context) 
{
    $url = new moodle_url('/course/report/sofia/index.php', array('id' => $course->id));
    $navigation->add(get_string('sofia', 'report_sofia'), $url);
}


function sofia_report_get_attendee($values)
{
    global $DB;

    $sql = '
        SELECT 
            atl.id,
            us.firstname, 
            us.lastname, 
            ats.sessdate,
            stg.acronym,
            stg.description
        FROM {attendance} att
        JOIN {attendance_sessions} ats on att.id = ats.attendanceid
        JOIN {attendance_log} atl ON (atl.sessionid = ats.id)
        JOIN {attendance_statuses} stg ON (stg.id = atl.statusid AND stg.deleted = 0 AND stg.visible = 1)
        JOIN {user} us on us.id = atl.studentid
        WHERE 1
            AND ats.lasttakenby != 0
            AND att.course = :id
            AND ats.sessdate >= :startdate
            AND ats.sessdate <= :enddate
    ';
    $arr = [];
    $data = $DB->get_records_sql($sql, [ 
        'id' => $values->id, 
        'startdate' => $values->from, 
        'enddate' => $values->to 
    ]);
    foreach ($data as $key => $value)
    {
        $date = userdate($value->sessdate, get_string('strftimerecent'));

        $arr[] = [$date , $value->firstname . ' ' . $value->lastname, $value->acronym, $value->description];
    }
    $result = json_encode($arr);
    return $result;
}

function sofia_report_get_grades($id)
{
    global $DB;

    $sql = '
        SELECT
            gg.id, 
            c.shortname shortname, 
            gi.itemtype,
            CONCAT(mu.firstname, \' \', mu.lastname) AS userfullname, 
            gg.finalgrade AS finalgrade
        FROM {grade_items} AS gi
        INNER JOIN {course} c ON c.id = gi.courseid
        LEFT JOIN {grade_grades} AS gg ON gg.itemid = gi.id
        INNER JOIN {user} AS mu ON gg.userid = mu.id
        WHERE c.id = ?
    ';
    $arr = [];
    $data = $DB->get_records_sql($sql, [$id]);
    foreach ($data as $key => $value)
    {
        $arr[] = [ (int)$value->id, $value->shortname, $value->itemtype, $value->userfullname, (int)$value->finalgrade];
    }
    
    $result = json_encode($arr);
    return $result;

}

function sofia_report_get_user_data()
{
    global $DB;

    $sql = '
        SELECT c.name, COUNT(c.id) count
        FROM {cohort} c
        JOIN {cohort_members} m ON m.cohortid = c.id
        GROUP BY c.id
    ';
    $data = $DB->get_records_sql_menu($sql);
    $arr = [ [ 'Name', 'Count' ] ];
    foreach ($data as $key => $value)
    {
    	$arr[] = [$key, (int)$value];
    }

    $result = json_encode($arr);
    return $result;
}

function sofia_report_get_enrol_qualification()
{
    global $DB;

    $sql = '
        SELECT a.id, a.fullname , b.id bid, b.fullname name
        FROM 
        (
            SELECT b.id, courseid, b.fullname, customint7
            FROM {enrol} a
            join {course} b on a.courseid = b.id
            where enrol = \'qualification\'
        ) a
        JOIN {course} b on a.customint7 = b.id
    ';
    $data = $DB->get_records_sql($sql);
    foreach ($data as $key => $value)
    {
        $arr[] = [ (int)$value->id, $value->fullname, (int)$value->bid, $value->name];
    }
    $result = json_encode($arr);
    return $result;
}