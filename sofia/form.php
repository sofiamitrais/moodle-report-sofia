<?php

//moodleform is defined in formslib.php
require_once("$CFG->libdir/formslib.php");
 
class enrollment_form extends moodleform {

    //Add elements to form
    public function definition() {
        global $CFG;
 
        $mform = $this->_form; // Don't forget the underscore! 

        $mform->addElement('date_selector', 'from', get_string('from'));
        $mform->setDefault('from', time() - (3600 * 24 * 30));

        $mform->addElement('date_selector', 'to', get_string('to'));

        $courses = get_courses();
        $options = [];
        foreach ($courses as $key => $val)
        {
            $options[$val->id] = $val->shortname . ' - ' . $val->fullname;
        }
        asort($options);
        $mform->addElement('select', 'id', get_string('course'), $options);

        $this->add_action_buttons(false, get_string('search'));
    }

    function get_values() 
    {
        $mform =& $this->_form;

        $data = $mform->exportValues();
        unset($data['sesskey']); // we do not need to return sesskey
        unset($data['_qf__'.$this->_formname]);   // we do not need the submission marker too
        if (empty($data)) {
            return NULL;
        } else {
            return (object)$data;
        }
    }

    
    //Custom validation should be added here
    function validation($data, $files) 
    {
        return array();
    }
}