<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     report_sofia
 * @category    string
 * @copyright   2017 Sofia
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Report Sofia';
$string['sofia'] = 'Sofia';
$string['totalcoursepercategory'] = 'Total Course per category';

$string['course_per_cohort'] = 'Course per Cohort';
$string['course_qualification'] = 'Course Qualification';
$string['training_attendee'] = 'Training Attendee';
$string['training_grades'] = 'Training Grades';
$string['grade_id'] = 'Grade Id';
$string['grade_type'] = 'Grade Item Type';