
<?php

require_once('../../config.php');
require_once('lib.php');


$PAGE->set_url('/report/sofia/grades.php');

    
//if not id
require_login();
$context = context_system::instance();
$PAGE->set_context($context);

$PAGE->set_title(get_string('training_grades', 'report_sofia')); 
$PAGE->set_heading(get_string('report')); // set heading

echo $OUTPUT->header();

$menu = 'grades';
 
$courses = get_courses();
$options = [];
foreach ($courses as $key => $val)
{
    $options[$val->id] = $val->shortname . ' - ' . $val->fullname;
}
asort($options);
$courseId = empty($_GET['id'])? key($options) : $_GET['id']; 
?>

<?php  require_once(dirname(__FILE__) . '/includes/header.php');  ?>

<div style="margin-top: 30px;">
<form method="get">
	Course : 
	<select class="custom-select" name="id">
	<?php
		foreach($options as $key => $val)
		{
			?>
			<option value="<?php echo $key; ?>"<?php echo $courseId == $key? ' selected="selected"' : ''; ?>>
				<?php echo $val; ?>
			</option>
			<?php
		}
	?>
	</select>
	<button class="btn btn-success">View</button>
</form>
</div>
<div id="table_div" style="margin-top: 30px;"></div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {packages:['table']});
	google.charts.setOnLoadCallback(drawChart);
	function drawChart() 
	{
		var data = new google.visualization.DataTable();
		data.addColumn('number', '<?php echo get_string('grade_id', 'report_sofia') ?>');
		data.addColumn('string', '<?php echo get_string('course') ?>');
		data.addColumn('string', '<?php echo get_string('grade_type', 'report_sofia') ?>');
		data.addColumn('string', '<?php echo get_string('name') ?>');
		data.addColumn('number', '<?php echo get_string('grade') ?>');
		data.addRows(<?php echo sofia_report_get_grades($courseId); ?>);
		var table = new google.visualization.Table(document.getElementById('table_div'));
		table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
	}
</script>

<?php
echo $OUTPUT->footer();
