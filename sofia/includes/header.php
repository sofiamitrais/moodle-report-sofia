<ul class="nav nav-tabs" style="margin: 0;">
	<li class="nav-item">
	<a class="nav-link<?php echo $menu == 'course'? ' active' : ''; ?>" href="<?php echo $CFG->wwwroot ?>/report/sofia/index.php">
		<?php echo get_string('course_per_cohort', 'report_sofia'); ?>
	</a>
	</li>
	<li class="nav-item">
	<a class="nav-link<?php echo $menu == 'qualification'? ' active' : ''; ?>" href="<?php echo $CFG->wwwroot ?>/report/sofia/qualification.php">
		<?php echo get_string('course_qualification', 'report_sofia'); ?>
	</a>
	</li>
	<li class="nav-item">
	<a class="nav-link<?php echo $menu == 'enrollment'? ' active' : ''; ?>" href="<?php echo $CFG->wwwroot ?>/report/sofia/enrollment.php">
		<?php echo get_string('training_attendee', 'report_sofia'); ?>
	</a>
	</li>
	<li class="nav-item">
	<a class="nav-link<?php echo $menu == 'grades'? ' active' : ''; ?>" href="<?php echo $CFG->wwwroot ?>/report/sofia/grades.php">
		<?php echo get_string('training_grades', 'report_sofia'); ?>
	</a>
	</li>
</ul>
