<?php

require_once('../../config.php');
require_once('lib.php');


$PAGE->set_url('/report/sofia/index.php');

    
//if not id
require_login();
$context = context_system::instance();
$PAGE->set_context($context);

$PAGE->set_title(get_string('course_per_cohort', 'report_sofia')); 
$PAGE->set_heading(get_string('report')); // set heading

echo $OUTPUT->header();

$menu = 'course';
?>

<?php  require_once(dirname(__FILE__) . '/includes/header.php');  ?>

<div id="donutchart" style="width: 900px; height: 500px;"></div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {packages:['corechart']});
	google.charts.setOnLoadCallback(drawChart);
	function drawChart() 
	{
		var data = google.visualization.arrayToDataTable(<?php echo sofia_report_get_user_data(); ?>);

		var options = {
			title: '<?php echo get_string('totalcoursepercategory', 'report_sofia') ?>'
		};

		var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
		chart.draw(data, options);
	}
    </script>


<?php
echo $OUTPUT->footer();
