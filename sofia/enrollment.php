
<?php

require_once('../../config.php');
require_once('lib.php');
require_once('form.php');

$PAGE->set_url('/report/sofia/index.php');

    
//if not id
require_login();
$context = context_system::instance();
$PAGE->set_context($context);

$PAGE->set_title(get_string('training_attendee', 'report_sofia')); 
$PAGE->set_heading(get_string('report')); // set heading

echo $OUTPUT->header();

$menu = 'enrollment';
?>

<?php  require_once(dirname(__FILE__) . '/includes/header.php');  ?>

<div style="margin-top: 30px;">
<?php

	//Instantiate simplehtml_form 
	$mform = new enrollment_form();
	$mform->display();

	$values = $mform->get_values();
?>
</div>

<div id="table_div" style="margin-top: 30px;"></div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {packages:['table']});
	google.charts.setOnLoadCallback(drawChart);
	function drawChart() 
	{
		var data = new google.visualization.DataTable();
		data.addColumn('string', '<?php echo get_string('time') ?>');
		data.addColumn('string', '<?php echo get_string('name') ?>');
		data.addColumn('string', '<?php echo get_string('status') ?>');
		data.addColumn('string', '<?php echo get_string('description') ?>');
		data.addRows(<?php echo sofia_report_get_attendee($values); ?>);
		var table = new google.visualization.Table(document.getElementById('table_div'));
		table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
	}
</script>

<?php
echo $OUTPUT->footer();
